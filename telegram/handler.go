package telegram

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"strings"
	"sync"

	"github.com/caiofilipini/go-against-humanity/game"
	"github.com/caiofilipini/go-against-humanity/store"
)

const (
	emojiHeart  = "\U00002764"
	emojiCookie = "\U0001F36A"
)

// Handler is the HTTP handler for the Telegram bot.
type Handler struct {
	bot             *botAPI
	games           sync.Map
	usersToPlayers  sync.Map
	playersToUsers  sync.Map
	usersToGames    sync.Map
	inFlightAnswers sync.Map
	commands        map[string]command
	store           store.Store
}

// NewHandler creates and returns a new Handler with the given token.
// It returns an error in case it fails to restore games from the
// data storage.
func NewHandler(telegramAPI string, token string, store store.Store) (*Handler, error) {
	h := &Handler{
		bot: &botAPI{
			url:   fmt.Sprintf("%s%s", telegramAPI, token),
			token: token,
		},
		store: store,
	}

	if err := h.restoreGames(); err != nil {
		return nil, err
	}

	commands := map[string]command{
		"/newgame":      h.newGame,
		"/startgame":    h.startRound,
		"/restartgame":  h.restartGame,
		"/restartround": h.restartRound,
		"/gamestatus":   h.gameStatus,
		"/showscores":   h.showScores,
		"/join":         h.join,
		"/leave":        h.leave,
		"/start":        h.playerStart,
	}
	h.commands = commands

	return h, nil
}

// Router returns the http.Handler configured with all the routes
// for handling Bot messages.
func (h *Handler) Router() http.Handler {
	router := http.NewServeMux()
	router.HandleFunc(fmt.Sprintf("/%s/msg", h.bot.token), h.handleMsg)
	return router
}

func (h *Handler) handleMsg(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	if r.Method != "POST" {
		http.Error(w, "only POST allowed", http.StatusMethodNotAllowed)
		return
	}

	// TODO: remove the dump
	dump, derr := httputil.DumpRequest(r, true)
	if derr != nil {
		log.Printf("ERR: %v\n", derr)
		return
	}
	log.Println()
	log.Println(string(dump))
	log.Println()

	var upd update
	err := json.NewDecoder(r.Body).Decode(&upd)
	if err != nil {
		log.Printf("failed to parse request body: %v\n", err)
		http.Error(w, "invalid request body", http.StatusBadRequest)
		return
	}

	msg := strings.TrimSpace(upd.Message.Text)
	log.Println("received msg:", msg)

	if msg == "" {
		log.Println("empty message, skipping")
		return
	}

	cmd := strings.Split(upd.Message.Text, " ")[0]

	if strings.Contains(cmd, "@") {
		cmd = strings.Split(cmd, "@")[0]
	}

	var rep reply
	if cmdHandler, ok := h.commands[cmd]; ok {
		log.Printf("executing %s\n", cmd)

		rep, err = cmdHandler(upd.Message)
		if err != nil {
			log.Printf("error while handling %s: %v\n", cmd, err)
			return
		}
	} else {
		if upd.Message.Chat.Type == "private" {
			// This is likely a reply from a user, so let's handle it.
			h.handleAnswer(upd.Message)
			return
		}

		log.Printf("%s is not a recognized command\n", cmd)
		return
	}

	if rep.Text != "" {
		err = h.bot.sendMsg(rep)
		if err != nil {
			log.Printf("failed to send message: %v\n", err)
		}
	}
}

func (h *Handler) findCzarsUserID(czar *game.Player) (int64, error) {
	// TODO: replace with playersToUsers
	var czarsUserID int64
	h.usersToPlayers.Range(func(u interface{}, p interface{}) bool {
		uid := u.(int64)
		pl := p.(*game.Player)

		if pl.ID() == czar.ID() {
			czarsUserID = uid
			return false
		}

		return true
	})

	if czarsUserID == 0 {
		return 0, errors.New("failed to find Czar's user ID")
	}

	return czarsUserID, nil
}

func (h *Handler) saveGame(g *game.Game, gameChatID int64) {
	log.Printf("saving game %s\n", g.ID())

	save := g.Export()

	// Set ChatID for the game and all players before saving.
	save.ChatID = gameChatID

	for _, pl := range save.Players {
		playerChatID, ok := h.playersToUsers.Load(pl.ID)
		if !ok {
			continue
		}

		pl.ChatID = playerChatID.(int64)
	}

	if err := h.store.Put(store.Key(g.ID()), save); err != nil {
		log.Printf("failed to save game %s: %v\n", g.ID(), err)
		return
	}

	log.Printf("game %s successfully saved\n", g.ID())
}

func (h *Handler) restoreGames() error {
	games, err := h.store.All()
	if err != nil {
		return err
	}

	for _, saveGame := range games {
		log.Printf("restoring state for game %s\n", saveGame.ID)

		gameChatID := saveGame.ChatID
		if gameChatID == 0 {
			log.Printf("failed to restore game %s: no chat ID, skipping\n", gameChatID)
			continue
		}

		g := game.Import(saveGame)
		h.games.Store(gameChatID, g)

		for _, p := range saveGame.Players {
			h.usersToGames.Store(p.ChatID, saveGame.ChatID)

			if player := g.FindPlayer(p.ID); player != nil {
				h.usersToPlayers.Store(p.ChatID, player)
				h.playersToUsers.Store(player.ID(), p.ChatID)
			}
		}

		err = h.bot.sendMsg(reply{
			ChatID: gameChatID,
			Text:   fmt.Sprintf("Looks like I died a quick death, but luckily I still had a %s  available!\nI've restored the state of your game, but unfortunately I will need to start a new round.\nI apologize. Here, take this %s .", emojiHeart, emojiCookie),
		})
		if err != nil {
			log.Printf("failed to notify chat %d of game restore: %v\n", gameChatID, err)
		}

		// Start new round for restored game.
		if err := h.bot.sendMsg(h.newRound(gameChatID, false)); err != nil {
			log.Printf("failed to start new round: %v\n", err)
		}

		log.Printf("game %s successfully restored\n", saveGame.ID)
	}

	return nil
}
