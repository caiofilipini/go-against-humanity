package telegram

import (
	"fmt"
	"log"
	"sort"
	"strings"

	"github.com/caiofilipini/go-against-humanity/game"
	"github.com/pkg/errors"
)

const (
	answersSeparator = " || "
)

// command is a special handler that knows how to deal with
// bot commands. A nil error indicates the reply returned
// should be sent. A non-nil error returned indicates the
// reply should be ignored and the error should be logged.
type command func(msg message) (reply, error)

type replier func(game *game.Game) reply

func (h *Handler) startRound(msg message) (reply, error) {
	return h.newRound(msg.Chat.ID, false), nil
}

func (h *Handler) restartRound(msg message) (reply, error) {
	return h.newRound(msg.Chat.ID, true), nil
}

func (h *Handler) newGame(msg message) (reply, error) {
	chatID := msg.Chat.ID
	if _, ok := h.games.Load(chatID); ok {
		return reply{
			ChatID: chatID,
			Text:   "There is already an active game in this chat.",
		}, nil
	}

	game := game.NewGame()
	h.games.Store(chatID, game)

	return reply{
		ChatID: chatID,
		Text:   fmt.Sprintf("Game %s was successfully created.", game.ID()),
	}, nil
}

func (h *Handler) restartGame(msg message) (reply, error) {
	chatID := msg.Chat.ID
	return h.withActiveGame(chatID, func(game *game.Game) reply {
		if err := game.Restart(); err != nil {
			return reply{
				ChatID: chatID,
				Text:   err.Error(),
			}
		}

		h.games.Store(chatID, game)
		h.saveGame(game, chatID)

		return h.newRound(chatID, false)
	}), nil
}

func (h *Handler) gameStatus(msg message) (reply, error) {
	return h.withActiveGame(msg.Chat.ID, func(game *game.Game) reply {
		return reply{
			ChatID: msg.Chat.ID,
			Text:   game.Status(),
		}
	}), nil
}

func (h *Handler) showScores(msg message) (reply, error) {
	return h.withActiveGame(msg.Chat.ID, func(game *game.Game) reply {
		return reply{
			ChatID: msg.Chat.ID,
			Text:   game.ShowScores(),
		}
	}), nil
}

func (h *Handler) join(msg message) (reply, error) {
	return h.withActiveGame(msg.Chat.ID, func(g *game.Game) reply {
		player := game.NewPlayer(fmt.Sprintf("%s %s", msg.From.FirstName, msg.From.LastName))

		if _, ok := h.usersToGames.Load(msg.From.ID); ok {
			return reply{
				ChatID: msg.Chat.ID,
				Text:   fmt.Sprintf("%s is already playing.", player.Name()),
			}
		}

		g.AddPlayer(player)

		h.usersToPlayers.Store(msg.From.ID, player)
		h.playersToUsers.Store(player.ID(), msg.From.ID)
		h.usersToGames.Store(msg.From.ID, msg.Chat.ID)

		h.bot.notifyPlayer(player, msg.From.ID, msg.Chat.ID)

		h.saveGame(g, msg.Chat.ID)

		return reply{
			ChatID: msg.Chat.ID,
			Text:   fmt.Sprintf("%s joined.", player.Name()),
		}
	}), nil
}

func (h *Handler) leave(msg message) (reply, error) {
	return h.withActiveGame(msg.Chat.ID, func(g *game.Game) reply {
		pl, ok := h.usersToPlayers.Load(msg.From.ID)
		if !ok {
			log.Printf("can't find player with user ID %d\n", msg.From.ID)
			return reply{}
		}

		player := pl.(*game.Player)
		if err := g.RemovePlayer(player); err != nil {
			return reply{
				ChatID: msg.Chat.ID,
				Text:   err.Error(),
			}
		}

		h.usersToPlayers.Delete(msg.From.ID)
		h.usersToGames.Delete(msg.From.ID)

		h.saveGame(g, msg.Chat.ID)

		return reply{
			ChatID: msg.Chat.ID,
			Text:   fmt.Sprintf("%s left.", player.Name()),
		}
	}), nil
}

func (h *Handler) handleAnswer(msg message) error {
	playerChatID := msg.From.ID
	chatID, ok := h.usersToGames.Load(playerChatID)
	if !ok {
		return fmt.Errorf("can't find game for user %d", msg.From.ID)
	}

	gameChatID := chatID.(int64)
	g, err := h.loadGame(gameChatID)
	if err != nil {
		return errors.Wrapf(err, "can't find game for chat %d", msg.Chat.ID)
	}

	round := g.CurrentRound()
	if round == nil {
		return fmt.Errorf("no current round for game %s", g.ID())
	}

	pl, ok := h.usersToPlayers.Load(msg.From.ID)
	if !ok {
		return fmt.Errorf("can't find player for user ID %d", msg.From.ID)
	}

	player := pl.(*game.Player)
	answerText := msg.Text

	if player.ID() == round.Czar().ID() {
		log.Printf("received Czar's pick: %s\n", answerText)

		return h.handleCzarsPick(g, gameChatID, answerText)
	}

	log.Printf("received %s's answer: %s\n", player.Name(), answerText)

	return h.handlePlayersPick(player, round, playerChatID, gameChatID, answerText)
}

func (h *Handler) playerStart(msg message) (reply, error) {
	return reply{
		ChatID: msg.From.ID,
		Text:   "You're all set!",
		ReplyMarkup: &replyMarkup{
			RemoveKeyboard: true,
		},
	}, nil
}

func (h *Handler) loadGame(chatID int64) (*game.Game, error) {
	g, ok := h.games.Load(chatID)
	if !ok {
		return nil, errors.New("No active games for this chat.")
	}

	game, ok := g.(*game.Game)
	if !ok {
		return nil, errors.New("Invalid game.")
	}

	return game, nil
}

func (h *Handler) withActiveGame(chatID int64, replyFn replier) reply {
	game, err := h.loadGame(chatID)
	if err != nil {
		return reply{
			ChatID: chatID,
			Text:   err.Error(),
		}
	}
	return replyFn(game)
}

func (h *Handler) newRound(chatID int64, allowOverride bool) reply {
	return h.withActiveGame(chatID, func(g *game.Game) reply {
		if !allowOverride {
			if g.CurrentRound() != nil && !g.CurrentRound().IsFinished() {
				return reply{
					ChatID: chatID,
					Text:   "Current round hasn't finished yet.",
				}
			}
		}

		round, err := g.NewRound()
		var status string
		if err != nil {
			if g.Finished() {
				winners := g.Winners()

				var winnerText string
				if len(winners) > 1 {
					winnerNames := make([]string, len(winners))
					for i, w := range winners {
						winnerNames[i] = w.Name()
					}
					winnerText = fmt.Sprintf("There's a draw! The winners are *%s*!", strings.Join(winnerNames, ", "))
				} else {
					winnerText = fmt.Sprintf("The winner is *%s*!", winners[0].Name())
				}

				return reply{
					ChatID:    chatID,
					Text:      fmt.Sprintf("Not enough white cards to continue, finishing the game.\n\n%s\n\nIf you wish to keep playing, please send a /restartgame command.", winnerText),
					ParseMode: markdown,
				}
			}

			status = err.Error()
		} else {
			status = round.String()

			h.usersToPlayers.Range(func(k, v interface{}) bool {
				playerChatID := k.(int64)
				player := v.(*game.Player)
				var pvtMsg reply

				// We shouldn't send answers to the Czar, so let's
				// send the question instead.
				if player.ID() == round.Czar().ID() {
					pvtMsg = reply{
						ChatID: playerChatID,
						Text:   fmt.Sprintf("Your question:\n%s", round.BlackCard().Text),
						ReplyMarkup: &replyMarkup{
							RemoveKeyboard: true,
						},
					}
				} else {
					blackCard := round.BlackCard()
					helpText := ""

					if blackCard.Pick > 1 {
						helpText = fmt.Sprintf(" (choose %d answers)", blackCard.Pick)
					}

					pvtMsg = h.bot.makeReplyWithButtons(
						cardsToTexts(player.Cards()),
						playerChatID,
						fmt.Sprintf("%s asks%s:\n%s",
							round.Czar(),
							helpText,
							round.BlackCard().Text,
						),
					)
				}

				if err := h.bot.sendPvt(player, pvtMsg, chatID); err != nil {
					log.Printf("failed to send message to player %s: %v\n", player, err)
				}

				return true
			})
		}

		return reply{
			ChatID: chatID,
			Text:   status,
		}
	})
}

func (h *Handler) handleCzarsPick(g *game.Game, gameChatID int64, answerText string) error {
	cardTexts := strings.Split(answerText, answersSeparator)
	cards := make([]game.WhiteCard, len(cardTexts))
	for i, text := range cardTexts {
		cards[i] = game.WhiteCardFromText(text)
	}
	log.Printf("Czar picked: %v\n", cards)

	round := g.CurrentRound()
	if err := round.CzarPicked(cards); err != nil {
		log.Printf("failed to store Czar's picks: %v\n", err)
		return errors.Wrap(err, "failed to store Czar's picks")
	}

	if err := round.Finish(); err != nil {
		return errors.Wrap(err, "failed to finish round")
	}

	// Notify channel of the results.
	err := h.bot.sendMsg(reply{
		ChatID: gameChatID,
		Text: fmt.Sprintf("%s picked: the winner of this round is *%s*!\n\n%s\n%s",
			round.Czar(),
			round.Winner(),
			round.Status(),
			g.ShowScores(),
		),
		ParseMode: markdown,
	})
	if err != nil {
		log.Printf("failed to send message: %v\n", err)
	}

	h.saveGame(g, gameChatID)

	// Start new round.
	return h.bot.sendMsg(h.newRound(gameChatID, false))
}

func (h *Handler) handlePlayersPick(player *game.Player, round *game.Round, playerChatID int64, gameChatID int64, answerText string) error {
	card, err := player.CardByText(answerText)
	if err != nil {
		return fmt.Errorf("can't find %s's answer: %s", answerText)
	}

	czar := round.Czar()
	czarsChatID, err := h.findCzarsUserID(&czar)
	if err != nil {
		return errors.Wrap(err, "can't find Czar's chat ID")
	}

	expectedPicks := round.BlackCard().Pick
	var cards []game.WhiteCard

	log.Printf("it's a %d picks question\n", expectedPicks)

	if expectedPicks > 1 {
		combinedChatID := gameChatID + playerChatID

		var answers []game.WhiteCard
		a, ok := h.inFlightAnswers.Load(combinedChatID)
		if !ok {
			answers = []game.WhiteCard{card}
		} else {
			// Store the new pick.
			answers = append(a.([]game.WhiteCard), card)

		}

		log.Printf("this is %s's pick #%d\n", player, len(answers))

		if len(answers) == expectedPicks {
			// All picks received, let's remove the in flight answers for
			// this player and move on.
			cards = answers

			h.inFlightAnswers.Delete(combinedChatID)
		} else {
			// Otherwise, let's store the current player's pick and
			// prompt them for the next pick.
			h.inFlightAnswers.Store(combinedChatID, answers)

			log.Printf("prompting %s to pick their next answer\n", player)

			pvtMsg := h.bot.makeReplyWithButtons(
				cardsToTexts(player.CardsExcluding(answers)),
				playerChatID,
				"Pick your next card:",
			)

			err = h.bot.sendPvt(player, pvtMsg, gameChatID)
			if err != nil {
				log.Printf("failed to prompt %s to pick their next answer: %v\n", player, err)
			}

			return nil
		}
	} else {
		// It's a single pick question, so let's skip the whole
		// in flight dance and go straight to notifying the game.
		cards = []game.WhiteCard{card}
	}

	if err := round.PlayerPicked(player, game.WhiteCards(cards)); err != nil {
		return errors.Wrapf(err, "failed to store %s's answer", player)
	}

	// Notify player their answers were received, and
	// force Telegram to remove their keyboard.
	pvtMsg := reply{
		ChatID: playerChatID,
		Text:   fmt.Sprintf("Got your %d answer(s).", len(cards)),
		ReplyMarkup: &replyMarkup{
			RemoveKeyboard: true,
		},
	}
	if err := h.bot.sendPvt(player, pvtMsg, gameChatID); err != nil {
		log.Printf("failed to notify %s: %v\n", player, err)
	}

	if round.AllAnswersReceived() {
		// Notify channel that all answers have been received.
		err := h.bot.sendMsg(reply{
			ChatID: gameChatID,
			Text:   round.Status(),
		})
		if err != nil {
			log.Printf("failed to send message: %v\n", err)
		}

		// Prompt Czar to pick the winner.
		answers := round.Answers()
		var buttons []string
		for _, pAnswers := range answers {
			pCards := make([]string, len(pAnswers))
			for i, c := range pAnswers {
				pCards[i] = c.Text()
			}
			buttons = append(buttons, strings.Join(pCards, answersSeparator))
		}
		sort.Strings(buttons)

		pvtMsg := h.bot.makeReplyWithButtons(
			buttons,
			czarsChatID,
			fmt.Sprintf("Pick the best answer:\n%s",
				round.BlackCard().Text,
			),
		)

		return h.bot.sendPvt(&czar, pvtMsg, gameChatID)
	}

	return nil
}

func cardsToTexts(cards []game.WhiteCard) []string {
	texts := make([]string, len(cards))
	for i, c := range cards {
		texts[i] = c.Text()
	}
	return texts
}
