package telegram

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/caiofilipini/go-against-humanity/game"
	"github.com/pkg/errors"
)

const (
	// markdown is the Markdown parse mode used in the API.
	markdown = "Markdown"

	// maxRetries is the maximum number of times we will
	// try to send a message.
	maxRetries = 3

	// retryMultiplier is the multiplier used when calculating
	// the retry backoff sleep time.
	retryMultiplier = 1.5

	// retryMaxRandom is the maximum number of random milliseconds
	// used to introduce jitter in the retry backoff.
	retryMaxRandom = 200

	// retryBaseSleepInMillis is the number of milliseconds used as
	// the base for the retry backoff sleep time.
	retryBaseSleepInMillis = 250
)

type botAPI struct {
	token string
	url   string
}

func (b *botAPI) sendMsg(r reply) error {
	body, err := json.Marshal(r)
	if err != nil {
		return err
	}
	fmt.Printf("sending msg: %s\n", string(body))

	resp, err := withRetries(func() (*http.Response, error) {
		resp, err := http.Post(fmt.Sprintf("%s/sendMessage", b.url), "application/json", bytes.NewReader(body))
		if resp != nil {
			defer resp.Body.Close()
			io.Copy(ioutil.Discard, resp.Body)
		}
		return resp, err
	})
	if err != nil {
		return errors.Wrap(err, "failed to post message to Telegram")
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected respose: %d", resp.StatusCode)
	}
	return nil
}

func (b *botAPI) sendPvt(player *game.Player, pvtMsg reply, groupChatID int64) error {
	if err := b.sendMsg(pvtMsg); err != nil {
		log.Printf("failed to send private message to player %s: %v\n", player, err)
		log.Printf("will try to mention %s in the game chat\n", player)

		mention := reply{
			ChatID:    groupChatID,
			ParseMode: markdown,
			Text:      fmt.Sprintf("Hey [%s](tg://user?id=%d), I couldn't send you a private message. Please open a private chat with me and click *Start* so you can play.", player.Name(), pvtMsg.ChatID),
		}
		if err = b.sendMsg(mention); err != nil {
			return errors.Wrapf(err, "failed to send mention to player %s", player)
		}
	}
	return nil
}

func (b *botAPI) notifyPlayer(player *game.Player, playerChatID int64, groupChatID int64) {
	pvtMsg := reply{
		ChatID: playerChatID,
		Text:   "You're all set!",
		ReplyMarkup: &replyMarkup{
			RemoveKeyboard: true,
		},
	}
	if err := b.sendPvt(player, pvtMsg, groupChatID); err != nil {
		log.Printf("failed to notify %s: %v\n", player, err)
	}
}

func (b *botAPI) makeReplyWithButtons(buttonTexts []string, playerChatID int64, replyText string) reply {
	buttons := make([][]keyboardButton, len(buttonTexts))
	for i, text := range buttonTexts {
		buttons[i] = []keyboardButton{
			{
				Text: text,
			},
		}
	}

	return reply{
		ChatID: playerChatID,
		Text:   replyText,
		ReplyMarkup: &replyMarkup{
			Keyboard:        buttons,
			OneTimeKeyboard: true,
			ResizeKeyboard:  true,
		},
	}
}

// withRetries retries the given function up to maxRetries times in case it fails.
// It uses a naïve exponential backoff algorithm to calculate for how long to sleep
// in between tries. Here's the formula:
//
// sleep = baseSleep * multiplier * tryNumber + jitter
//
// Example:
//
// +-------+------------+------------+--------+--------+
// | Try # | Base sleep | Multiplier | Jitter | Sleep  |
// +-------+------------+------------+--------+--------+
// | 1     | 250ms      | 1.5        | 123ms  |  498ms |
// | 2     | 250ms      | 1.5        |  47ms  |  797ms |
// | 3     | 250ms      | 1.5        | 172ms  | 1297ms |
// +-------+------------+------------+--------+--------+
func withRetries(fn func() (*http.Response, error)) (resp *http.Response, err error) {
	try := 1

	for try <= maxRetries {
		resp, err = fn()
		if err != nil || resp.StatusCode >= 500 {
			jitter := rand.Intn(retryMaxRandom)
			sleep := (retryBaseSleepInMillis * retryMultiplier * try) + jitter

			log.Printf("try %d failed, sleeping for %d milliseconds\n", try, sleep)
			time.Sleep(time.Duration(sleep) * time.Millisecond)

			try++
			continue
		}
		return resp, err
	}

	log.Printf("%d retries exhausted, giving up\n", maxRetries)

	return resp, err
}

type update struct {
	ID      int64   `json:"id"`
	Message message `json:"message"`
}

type message struct {
	ID   int64  `json:"message_id"`
	Date int64  `json:"date"`
	Text string `json:"text"`
	From struct {
		ID           int64  `json:"id"`
		IsBot        bool   `json:"is_bot"`
		FirstName    string `json:"first_name"`
		LastName     string `json:"last_name"`
		LanguageCode string `json:"language_code"`
	} `json:"from"`
	Chat struct {
		ID        int64  `json:"id"`
		Title     string `json:"title"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Type      string `json:"type"`
	} `json:"chat"`
}

type reply struct {
	ChatID      int64        `json:"chat_id"`
	Text        string       `json:"text"`
	ParseMode   string       `json:"parse_mode,omitempty"`
	ReplyMarkup *replyMarkup `json:"reply_markup,omitempty"`
}

type replyMarkup struct {
	Keyboard        [][]keyboardButton `json:"keyboard,omitempty"`
	OneTimeKeyboard bool               `json:"one_time_keyboard,omitempty"`
	ResizeKeyboard  bool               `json:"resize_keyboard,omitempty"`
	RemoveKeyboard  bool               `json:"remove_keyboard,omitempty"`
}

type keyboardButton struct {
	Text string `json:"text"`
}
