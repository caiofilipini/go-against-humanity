package decks

// BlackCard represents a black card in the CAH game.
type BlackCard struct {
	Text string
	Pick int
}

// WhiteCard represents a white card in the CAH game.
type WhiteCard string

// Deck is a collection of black and white cards representing
// a CAH card deck.
type Deck struct {
	BlackCards []BlackCard
	WhiteCards []WhiteCard
}

// From takes a set of Decks and returns a combined set of
// BlackCards and WhiteCards.
func From(decks ...Deck) ([]BlackCard, []WhiteCard) {
	blacksCount, whitesCount := 0, 0

	for _, d := range decks {
		blacksCount += len(d.BlackCards)
		whitesCount += len(d.WhiteCards)
	}

	blacks := make([]BlackCard, 0, blacksCount)
	whites := make([]WhiteCard, 0, whitesCount)

	for _, d := range decks {
		for _, b := range d.BlackCards {
			blacks = append(blacks, b)
		}
		for _, w := range d.WhiteCards {
			whites = append(whites, w)
		}
	}

	return blacks, whites
}
