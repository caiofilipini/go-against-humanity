package game

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlayerPicked(t *testing.T) {
	tests := []struct {
		desc                string
		round               *Round
		player              *Player
		picks               WhiteCards
		expectedPlayerCards []WhiteCard
		expectedErr         error
	}{
		{
			desc: "stores player's single pick in the round",
			round: &Round{
				answers: make(map[string]WhiteCards),
				blackCard: BlackCard{
					Text: "Why do I hurt all over?",
					Pick: 1,
				},
			},
			player: &Player{
				id: "pl1",
				cards: WhiteCards{
					{text: "Rehab"},
					{text: "Used panties"},
				},
			},
			picks: WhiteCards{
				{text: "Rehab"},
			},
			expectedPlayerCards: []WhiteCard{
				{text: "Used panties"},
			},
		},
		{
			desc: "stores player's multiple picks in the round",
			round: &Round{
				answers: make(map[string]WhiteCards),
				blackCard: BlackCard{
					Text: "______ + ______ = ______.",
					Pick: 3,
				},
			},
			player: &Player{
				id: "pl1",
				cards: WhiteCards{
					{text: "Used panties"},
					{text: "Kanye West"},
					{text: "Rehab"},
					{text: "Grandma"},
					{text: "Being fat and stupid"},
				},
			},
			picks: WhiteCards{
				{text: "Rehab"},
				{text: "Being fat and stupid"},
				{text: "Used panties"},
			},
			expectedPlayerCards: []WhiteCard{
				{text: "Kanye West"},
				{text: "Grandma"},
			},
		},
		{
			desc: "returns an error when answers are incomplete",
			round: &Round{
				answers: make(map[string]WhiteCards),
				blackCard: BlackCard{
					Text: "______ + ______ = ______.",
					Pick: 3,
				},
			},
			player: &Player{
				id: "pl1",
				cards: WhiteCards{
					{text: "Used panties"},
					{text: "Kanye West"},
					{text: "Rehab"},
					{text: "Grandma"},
					{text: "Being fat and stupid"},
				},
			},
			picks: WhiteCards{
				{text: "Rehab"},
				{text: "Being fat and stupid"},
			},
			expectedErr: errInvalidAnswer,
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			err := tc.round.PlayerPicked(tc.player, tc.picks)
			picks, ok := tc.round.answers[tc.player.id]

			if tc.expectedErr != nil {
				assert.Equal(t, tc.expectedErr, err)
				assert.False(t, ok)
			} else {
				assert.NoError(t, err)
				assert.True(t, ok)

				assert.Equal(t, tc.picks, picks)
				assert.Equal(t, tc.expectedPlayerCards, tc.player.Cards())
			}
		})
	}
}

func TestCzarPicked(t *testing.T) {
	tests := []struct {
		desc         string
		round        *Round
		picks        []WhiteCard
		expectedPick string
		expectedErr  error
	}{
		{
			desc: "stores Czar's single pick in the round",
			round: &Round{
				answers: map[string]WhiteCards{
					"pl1": WhiteCards{
						{text: "Wet dreams"},
					},
					"pl2": WhiteCards{
						{text: "Rehab"},
					},
					"pl3": WhiteCards{
						{text: "Poor life choices"},
					},
				},
			},
			picks: []WhiteCard{
				{text: "Rehab"},
			},
			expectedPick: "pl2",
		},
		{
			desc: "stores Czar's multiple picks in the round",
			round: &Round{
				answers: map[string]WhiteCards{
					"pl1": WhiteCards{
						{text: "Wet dreams"},
						{text: "The art of seduction"},
					},
					"pl2": WhiteCards{
						{text: "Rehab"},
						{text: "Not wearing pants"},
					},
					"pl3": WhiteCards{
						{text: "Poor life choices"},
						{text: "Used panties"},
					},
				},
			},
			picks: []WhiteCard{
				{text: "Poor life choices"},
				{text: "Used panties"},
			},
			expectedPick: "pl3",
		},
		{
			desc: "returns an error for an empty list of answers",
			round: &Round{
				answers: map[string]WhiteCards{
					"pl1": WhiteCards{
						{text: "Wet dreams"},
					},
					"pl2": WhiteCards{
						{text: "Rehab"},
					},
					"pl3": WhiteCards{
						{text: "Poor life choices"},
					},
				},
			},
			picks:       []WhiteCard{},
			expectedErr: errInvalidPick,
		},
		{
			desc: "returns an error when the number of answers doesn't match",
			round: &Round{
				answers: map[string]WhiteCards{
					"pl1": WhiteCards{
						{text: "Wet dreams"},
						{text: "The art of seduction"},
					},
					"pl2": WhiteCards{
						{text: "Rehab"},
						{text: "Not wearing pants"},
					},
					"pl3": WhiteCards{
						{text: "Poor life choices"},
						{text: "Used panties"},
					},
				},
			},
			picks: []WhiteCard{
				{text: "Rehab"},
			},
			expectedErr: errInvalidAnswer,
		},
		{
			desc: "returns an error when single pick isn't found",
			round: &Round{
				answers: map[string]WhiteCards{
					"pl1": WhiteCards{
						{text: "Wet dreams"},
					},
					"pl2": WhiteCards{
						{text: "Rehab"},
					},
					"pl3": WhiteCards{
						{text: "Poor life choices"},
					},
				},
			},
			picks: []WhiteCard{
				{text: "Rebad"},
			},
			expectedErr: errPickNotFound,
		},
		{
			desc:  "returns an error when answers deck is empty",
			round: &Round{},
			picks: []WhiteCard{
				{text: "Rehab"},
			},
			expectedErr: errPickNotFound,
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			round := tc.round

			err := round.CzarPicked(tc.picks)

			assert.Equal(t, tc.expectedErr, err)
			assert.Equal(t, tc.expectedPick, round.czarsPick)
		})
	}
}

func TestIsFinished(t *testing.T) {
	tests := []struct {
		desc     string
		round    *Round
		expected bool
	}{
		{
			desc:     "returns false when no winner has been picked",
			round:    &Round{},
			expected: false,
		},
		{
			desc: "returns true when a winner has been picked",
			round: &Round{
				winner: &Player{id: "pl1"},
			},
			expected: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			assert.Equal(t, tc.expected, tc.round.IsFinished())
		})
	}
}

func TestFinish(t *testing.T) {
	tests := []struct {
		desc  string
		round *Round
	}{
		{},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
		})
	}
}
