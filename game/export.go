package game

import "github.com/caiofilipini/go-against-humanity/store"

// Export creates a new store.Game from the Game.
func (g *Game) Export() *store.Game {
	g.Lock()
	defer g.Unlock()

	sg := store.Game{
		ID:                  g.id,
		LastCzar:            g.lastCzar,
		WhiteCardsPerPlayer: g.whiteCardsPerPlayer,
		Players:             make([]*store.Player, len(g.players)),
		BlackCards:          make([]store.BlackCard, len(g.blackCards)),
		WhiteCards:          make([]string, len(g.whiteCards)),
	}

	for i, p := range g.players {
		sg.Players[i] = &store.Player{
			ID:    p.id,
			Name:  p.name,
			Score: p.score,
		}

		cards := make([]string, len(p.cards))
		for j, c := range p.cards {
			cards[j] = c.text
		}
		sg.Players[i].Cards = cards
	}

	for i, bc := range g.blackCards {
		sg.BlackCards[i] = store.BlackCard{
			Text: bc.Text,
			Pick: bc.Pick,
		}
	}

	for i, wc := range g.whiteCards {
		sg.WhiteCards[i] = wc.text
	}

	return &sg
}
