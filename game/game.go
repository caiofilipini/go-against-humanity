package game

import (
	"errors"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"

	"github.com/caiofilipini/go-against-humanity/game/decks"
	"github.com/caiofilipini/go-against-humanity/shuffle"

	"github.com/pborman/uuid"
)

const (
	defaultWhiteCardsPerPlayer = 10
	minPlayers                 = 3
)

var (
	errInvalidPick         = errors.New("invalid pick")
	errPickNotFound        = errors.New("cound't find Czar's pick in the deck")
	errFinishWithoutPick   = errors.New("can't finish game without Czar's pick")
	errBlackDeckEmpty      = errors.New("no more black cards")
	errIncompleteRound     = errors.New("round is incomplete")
	errPlayerNotFound      = errors.New("player not found")
	errCardNotFound        = errors.New("card not found")
	errWinnerUndefined     = errors.New("can't figure out the winner of the round")
	errInvalidAnswer       = errors.New("invalid number of picks")
	errNotEnoughPlayers    = fmt.Errorf("not enough players, need at least %d", minPlayers)
	errRoundFinished       = errors.New("round is already finished")
	errNotEnoughWhiteCards = errors.New("not enough whitecards")
	errGameNotFinished     = errors.New("can't restart a game that hasn't finished yet")
	errCzarCannotLeave     = errors.New("the czar cannot leave an active round")
)

// BlackCard represents a black card in the Cards Against Humanity
// game. Black cards are question cards that contain a question text
// and the number of answers expected for this question.
type BlackCard struct {
	Text string
	Pick int
}

// BlackCards represents a list of black cards. This type is
// mainly used for shuffling the cards when starting a new game.
type BlackCards []BlackCard

// Len returns the number of cards in the list.
func (b BlackCards) Len() int {
	return len(b)
}

// Swap swaps elements in indices i and j.
func (b BlackCards) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

// WhiteCard represents a white card in the Cards Against Humanity
// game. White cards are answer cards that contain an answer text.
type WhiteCard struct {
	text string
}

// WhiteCardFromText returns a WhiteCard with the given text.
func WhiteCardFromText(text string) WhiteCard {
	return WhiteCard{
		text: text,
	}
}

// Text returns the text associated with this WhiteCard.
func (w WhiteCard) Text() string {
	return w.text
}

// String returns a string representation of this WhiteCard
// (i.e. its text).
func (w WhiteCard) String() string {
	return w.Text()
}

// WhiteCards represents a list of white cards. This type is
// mainly used for shuffling the cards when starting a new game.
type WhiteCards []WhiteCard

// String returns a string representation of the list of white
// cards.
func (w WhiteCards) String() string {
	texts := make([]string, len(w))
	for i, c := range w {
		texts[i] = c.text
	}
	return strings.Join(texts, " || ")
}

// Len returns the number of cards in the list.
func (w WhiteCards) Len() int {
	return len(w)
}

// Swap swaps elements in indices i and j.
func (w WhiteCards) Swap(i, j int) {
	w[i], w[j] = w[j], w[i]
}

// Delete deletes the given WhiteCard from the list.
func (w *WhiteCards) Delete(wc WhiteCard) {
	newCards := make([]WhiteCard, 0, w.Len()-1)
	for _, c := range *w {
		if c.text != wc.text {
			newCards = append(newCards, c)
		}
	}
	*w = WhiteCards(newCards)
}

// Player represents a player in the game.
type Player struct {
	id    string
	name  string
	score int
	cards WhiteCards
}

// ID returns the UUID associated with this Player.
func (p Player) ID() string {
	return p.id
}

// Name returns the name associated with this Player.
func (p Player) Name() string {
	return p.name
}

// Cards returns the Player's current list of white cards.
// This function is safe and returns a copy of the original list.
func (p Player) Cards() []WhiteCard {
	cards := make([]WhiteCard, len(p.cards))
	for i, wc := range p.cards {
		cards[i] = wc
	}
	return cards
}

// CardsExcluding returns the Player's current list of white cards,
// excluding the cards that match the given WhiteCards.
// This function is safe and returns a copy of the original list.
// In case no cards matching the given WhiteCards are found, the
// returned list will be identical to the original list.
func (p Player) CardsExcluding(exclude []WhiteCard) []WhiteCard {
	cards := make([]WhiteCard, 0, len(p.cards))
	for _, wc := range p.cards {
		include := true
		for _, x := range exclude {
			if wc.text == x.text {
				include = false
				break
			}
		}
		if include {
			cards = append(cards, wc)
		}
	}
	return cards
}

// CardByText searches this Player's list of WhiteCards and returns
// the card matching the given text. It returns a non-nil error in
// case it's unable to find a matching card.
func (p Player) CardByText(text string) (WhiteCard, error) {
	for _, wc := range p.cards {
		if wc.text == text {
			return wc, nil
		}
	}
	return WhiteCard{}, errCardNotFound
}

// String returns a string representation of the Player.
func (p Player) String() string {
	return p.name
}

// NewPlayer creates and returns a new Player with the given name.
func NewPlayer(name string) *Player {
	return &Player{
		id:    uuid.New(),
		name:  name,
		score: 0,
	}
}

// Players represents a list of players in the game.
type Players []*Player

// String returns a string representation of the Players
// (a comma-separated list of their names (e.g. "Player 1, Player 2").
func (p Players) String() string {
	names := make([]string, len(p))
	for i, pl := range p {
		names[i] = pl.name
	}
	return strings.Join(names, ", ")
}

// find searches the list of players for a player with the
// given playerID. It returns a non-nil error in case it's
// unable to find a matching player.
func (p Players) find(playerID string) (*Player, error) {
	for _, pl := range p {
		if pl.id == playerID {
			return pl, nil
		}
	}
	return nil, errPlayerNotFound
}

// excluding returns the list of players excluding the given player.
// If it doesn't find a player matching the given player, the returned
// list will be equal to the original list.
func (p Players) excluding(player *Player) Players {
	remainingPlayers := make([]*Player, 0, len(p))
	for _, pl := range p {
		if pl.id == player.id {
			continue
		}
		remainingPlayers = append(remainingPlayers, pl)
	}
	return Players(remainingPlayers)
}

// sorted returns a copy of Players, sorted by score in descending
// order.
func (p Players) sorted() Players {
	plCopy := make([]*Player, len(p))
	for i, pl := range p {
		plCopy[i] = pl
	}
	sort.Slice(plCopy, func(i, j int) bool {
		return plCopy[i].score > plCopy[j].score
	})
	return Players(plCopy)
}

// leaders returns a list containing the players with the highest
// score. In case there's only one player at the top of the score
// board, the list will contain a single entry.
func (p Players) leaders() []Player {
	topScore := -1
	var leaders []Player

	for _, pl := range p.sorted() {
		if topScore == -1 {
			topScore = pl.score
		}

		if pl.score == topScore {
			leaders = append(leaders, *pl)
		}
	}

	return leaders
}

// Game represents a game of Cards Against Humanity.
type Game struct {
	id                  string
	whiteCardsPerPlayer int
	blackCards          BlackCards
	whiteCards          WhiteCards
	players             Players
	currentRound        *Round
	lastCzar            int
	finished            bool
	winners             []Player

	sync.Mutex
}

// NewGame creates and returns a new Game with fully populated
// decks of BlackCards and WhiteCards.
func NewGame() *Game {
	return newGameWithID(uuid.New())
}

// Restart resets the Game's scores, refills the white and black
// cards decks, and re-shuffles them. It returns a non-nil error
// in case the game hasn't yet finished (i.e. only a finished game
// can be restarted).
func (g *Game) Restart() error {
	if !g.Finished() {
		return errGameNotFinished
	}

	newGame := newGameWithID(g.id)

	for _, pl := range g.players {
		pl.cards = WhiteCards{}
		pl.score = 0

		newGame.AddPlayer(pl)
	}

	*g = *newGame

	return nil
}

// ID returns the UUID associated with the Game.
func (g *Game) ID() string {
	return g.id
}

// Finished returns whether or not the game has finished.
func (g *Game) Finished() bool {
	g.Lock()
	defer g.Unlock()
	return g.finished
}

// Winners returns the list of Players who won the game.
func (g *Game) Winners() []Player {
	g.Lock()
	defer g.Unlock()
	return g.winners
}

// Status returns a string indicating the current status of the game.
// If there is an active Round, the current round's status will be returned.
func (g *Game) Status() string {
	if g.finished {
		return "Game finished. Please start a new game."
	}
	if g.currentRound == nil {
		return "Game hasn't started yet. Please start a new round."
	}
	return g.currentRound.Status()
}

// ShowScores returns a string representation of the current game's
// scores in descending order (i.e. the player with the most points
// will be show on top).
func (g *Game) ShowScores() string {
	sortedPlayers := g.players.sorted()
	scores := make([]string, len(sortedPlayers))
	for i, p := range sortedPlayers {
		scores[i] = fmt.Sprintf("%s - %d points", p.name, p.score)
	}
	return strings.Join(scores, "\n")
}

// NewRound creates and returns a new Round. It takes care of dealing
// new WhiteCards for players who joined the game, choosing a new BlackCard
// for the round, as well as choosing the next Czar.
// It returns a non-nil error if there aren't enough players to start a round,
// or if the deck of black cards is empty.
func (g *Game) NewRound() (*Round, error) {
	g.Lock()
	defer g.Unlock()

	if len(g.players) < minPlayers {
		return nil, errNotEnoughPlayers
	}

	if err := g.dealWhiteCards(); err != nil {
		if err == errNotEnoughWhiteCards {
			g.currentRound = nil
			g.finished = true
			g.winners = g.players.leaders()
		}
		return nil, err
	}

	blackCard, err := g.nextBlackCard()
	if err != nil {
		return nil, err
	}

	nextCzar := g.nextCzar()
	roundPlayers := g.players.excluding(nextCzar)
	round := Round{
		id:        uuid.New(),
		czar:      nextCzar,
		players:   roundPlayers,
		blackCard: blackCard,
		answers:   make(map[string]WhiteCards),
	}

	g.currentRound = &round
	return g.currentRound, nil
}

// AddPlayer adds the given Player to the game.
// The Player will be included in the next round.
func (g *Game) AddPlayer(p *Player) {
	g.Lock()
	defer g.Unlock()

	g.players = append(g.players, p)

	log.Printf("%s (ID: %s) joined\n", p.name, p.id)
}

// RemovePlayer removes the given Player from the game.
// The Player will be excluded from the current and any
// future rounds. It returns a non-nil error in case the
// player being removed is the current round's czar.
func (g *Game) RemovePlayer(p *Player) error {
	g.Lock()
	defer g.Unlock()

	ind := -1
	for i, pl := range g.players {
		if pl.ID() == p.ID() {
			ind = i
			break
		}
	}
	if ind != -1 {
		// Remove player from round.
		if g.currentRound != nil {
			if err := g.currentRound.removePlayer(p); err != nil {
				return err
			}
		}

		// Remove player from list of players.
		g.players = append(g.players[:ind], g.players[ind+1:]...)

		log.Printf("%s (ID: %s) left\n", p.name, p.id)

		// Put the player's WhiteCards back into the deck.
		g.whiteCards = append(g.whiteCards, p.cards...)
	}

	return nil
}

// FindPlayer accepts a player ID and returns the corresponding
// player. It may return nil in case the given ID doesn't match
// any of the players.
func (g *Game) FindPlayer(id string) *Player {
	g.Lock()
	defer g.Unlock()

	for _, pl := range g.players {
		if pl.id == id {
			return pl
		}
	}

	return nil
}

// CurrentCzar returns the current round's czar.
func (g *Game) CurrentCzar() Player {
	g.Lock()
	defer g.Unlock()
	return *g.currentRound.czar
}

// CurrentRound returns the current active round. It may return
// nil in case no rounds have been started.
func (g *Game) CurrentRound() *Round {
	g.Lock()
	defer g.Unlock()
	return g.currentRound
}

// newGameWithID creates and returns a new Game with the given id,
// and fully populated decks of BlackCards and WhiteCards.
func newGameWithID(id string) *Game {
	allBlackCards, allWhiteCards := decks.From(
		decks.BaseDeck,
		decks.Expansion1Deck,
		decks.Expansion2Deck,
		decks.Expansion3Deck,
		decks.Expansion4Deck,
		decks.Expansion5Deck,
		decks.Expansion6Deck,
	)

	bc := make([]BlackCard, len(allBlackCards))
	for i, c := range allBlackCards {
		bc[i] = BlackCard{
			Text: c.Text,
			Pick: c.Pick,
		}
	}
	blackCards := BlackCards(bc)
	shuffle.Shuffle(blackCards)

	wc := make([]WhiteCard, len(allWhiteCards))
	for i, c := range allWhiteCards {
		wc[i] = WhiteCard{
			text: string(c),
		}
	}
	whiteCards := WhiteCards(wc)
	shuffle.Shuffle(whiteCards)

	return &Game{
		id:                  id,
		whiteCardsPerPlayer: defaultWhiteCardsPerPlayer,
		blackCards:          blackCards,
		whiteCards:          whiteCards,
		players:             Players{},
		lastCzar:            -1,
	}
}

// nextCzar chooses and returns a czar for the next round.
// It's called from NewRound(), which should already have locked the mutex, so
// no need to lock again here.
func (g *Game) nextCzar() *Player {
	nextCzar := g.lastCzar + 1
	if nextCzar == len(g.players) {
		nextCzar = 0
	}
	czar := g.players[nextCzar]
	g.lastCzar = nextCzar
	return czar
}

// dealWhiteCards checks how many cards are missing for each player, and deals
// new cards to replace them.
// It returns a non-nil error in case there aren't enough white cards left
// for a new round to be played.
// It's called from NewRound(), which should already have locked the mutex, so
// no need to lock again here.
func (g *Game) dealWhiteCards() error {
	for _, p := range g.players {
		missing := g.whiteCardsPerPlayer - len(p.cards)

		// Check whether or not there are enough white cards
		// for a new round and, if not, return an error.
		if missing > len(g.whiteCards) {
			return errNotEnoughWhiteCards
		}

		cards := g.whiteCards[0:missing]
		g.whiteCards = g.whiteCards[missing:]

		p.cards = append(p.cards, cards...)
	}

	return nil
}

// nextBlackCard selects the next available black card, removes it from
// the deck and returns it. It returns a non-nil error in case the black
// cards deck is empty.
// It's called from NewRound(), which should already have locked the mutex, so
// no need to lock again here.
func (g *Game) nextBlackCard() (BlackCard, error) {
	if len(g.blackCards) == 0 {
		return BlackCard{}, errBlackDeckEmpty
	}
	chosen := g.blackCards[0]
	g.blackCards = g.blackCards[1:]

	return chosen, nil
}
