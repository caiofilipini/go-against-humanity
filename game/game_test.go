package game

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPlayersExcluding(t *testing.T) {
	tests := []struct {
		desc     string
		players  []*Player
		czar     *Player
		expected Players
	}{
		{
			desc: "returns list of players without czar",
			players: []*Player{
				&Player{id: "pl1"},
				&Player{id: "pl2"},
				&Player{id: "cz1"},
				&Player{id: "pl3"},
			},
			czar: &Player{id: "cz1"},
			expected: Players([]*Player{
				&Player{id: "pl1"},
				&Player{id: "pl2"},
				&Player{id: "pl3"},
			}),
		},
		{
			desc: "returns full list when czar isn't found",
			players: []*Player{
				&Player{id: "pl1"},
				&Player{id: "pl2"},
				&Player{id: "pl3"},
			},
			czar: &Player{id: "cz1"},
			expected: Players([]*Player{
				&Player{id: "pl1"},
				&Player{id: "pl2"},
				&Player{id: "pl3"},
			}),
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			p := Players(tc.players)
			res := p.excluding(tc.czar)

			assert.Equal(t, tc.expected, res)
		})
	}
}

func TestPlayersLeaders(t *testing.T) {
	tests := []struct {
		desc     string
		players  Players
		expected []Player
	}{
		{
			desc:    "returns empty list of leaders when there are no players",
			players: Players{},
		},
		{
			desc: "returns a single leader when there's only one player",
			players: Players{
				&Player{
					score: 1,
					name:  "Marvin",
				},
			},
			expected: []Player{
				Player{
					score: 1,
					name:  "Marvin",
				},
			},
		},
		{
			desc: "returns a single leader when there's only one player with the highest score",
			players: Players{
				&Player{
					score: 1,
					name:  "Marvin",
				},
				&Player{
					score: 10,
					name:  "Ford",
				},
			},
			expected: []Player{
				Player{
					score: 10,
					name:  "Ford",
				},
			},
		},
		{
			desc: "returns multiple leaders when there's a draw at the highest score",
			players: Players{
				&Player{
					score: 1,
					name:  "Marvin",
				},
				&Player{
					score: 42,
					name:  "Ford",
				},
				&Player{
					score: 23,
					name:  "Zaphod",
				},
				&Player{
					score: 42,
					name:  "Arthur",
				},
			},
			expected: []Player{
				Player{
					score: 42,
					name:  "Ford",
				},
				Player{
					score: 42,
					name:  "Arthur",
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			assert.Equal(t, tc.expected, tc.players.leaders())
		})
	}
}

func TestNextCzar(t *testing.T) {
	tests := []struct {
		desc             string
		game             *Game
		expectedCzar     *Player
		expectedLastCzar int
	}{
		{
			desc: "returns the first player when lastCzar was unknown (-1)",
			game: &Game{
				lastCzar: -1,
				players: Players([]*Player{
					&Player{id: "pl1"},
					&Player{id: "pl2"},
					&Player{id: "pl3"},
					&Player{id: "pl4"},
				}),
			},
			expectedCzar:     &Player{id: "pl1"},
			expectedLastCzar: 0,
		},
		{
			desc: "returns the next player when lastCzar was the first player",
			game: &Game{
				lastCzar: 0,
				players: Players([]*Player{
					&Player{id: "pl1"},
					&Player{id: "pl2"},
					&Player{id: "pl3"},
					&Player{id: "pl4"},
				}),
			},
			expectedCzar:     &Player{id: "pl2"},
			expectedLastCzar: 1,
		},
		{
			desc: "returns the next player when lastCzar was a player in the middle of the list",
			game: &Game{
				lastCzar: 1,
				players: Players([]*Player{
					&Player{id: "pl1"},
					&Player{id: "pl2"},
					&Player{id: "pl3"},
					&Player{id: "pl4"},
				}),
			},
			expectedCzar:     &Player{id: "pl3"},
			expectedLastCzar: 2,
		},
		{
			desc: "returns the last player",
			game: &Game{
				lastCzar: 2,
				players: Players([]*Player{
					&Player{id: "pl1"},
					&Player{id: "pl2"},
					&Player{id: "pl3"},
					&Player{id: "pl4"},
				}),
			},
			expectedCzar:     &Player{id: "pl4"},
			expectedLastCzar: 3,
		},
		{
			desc: "returns the first player again when lastCzar was the last player",
			game: &Game{
				lastCzar: 3,
				players: Players([]*Player{
					&Player{id: "pl1"},
					&Player{id: "pl2"},
					&Player{id: "pl3"},
					&Player{id: "pl4"},
				}),
			},
			expectedCzar:     &Player{id: "pl1"},
			expectedLastCzar: 0,
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			assert.Equal(t, tc.expectedCzar, tc.game.nextCzar())
			assert.Equal(t, tc.expectedLastCzar, tc.game.lastCzar)
		})
	}
}

func TestNextBlackCard(t *testing.T) {
	tests := []struct {
		desc             string
		game             *Game
		expectedChoice   BlackCard
		expectedDeckLeft BlackCards
		expectedErr      error
	}{
		{
			desc: "pops card from the deck and returns it",
			game: &Game{
				blackCards: BlackCards([]BlackCard{
					{
						Text: "What is Batman's guilty pleasure?",
						Pick: 1,
					},
					{
						Text: "I drink to forget _.",
						Pick: 1,
					},
					{
						Text: "_. High five, bro.",
						Pick: 1,
					},
				}),
			},
			expectedChoice: BlackCard{
				Text: "What is Batman's guilty pleasure?",
				Pick: 1,
			},
			expectedDeckLeft: BlackCards([]BlackCard{
				{
					Text: "I drink to forget _.",
					Pick: 1,
				},
				{
					Text: "_. High five, bro.",
					Pick: 1,
				},
			}),
		},
		{
			desc: "pops the next card from the deck and returns it",
			game: &Game{
				blackCards: BlackCards([]BlackCard{
					{
						Text: "I drink to forget _.",
						Pick: 1,
					},
					{
						Text: "_. High five, bro.",
						Pick: 1,
					},
				}),
			},
			expectedChoice: BlackCard{
				Text: "I drink to forget _.",
				Pick: 1,
			},
			expectedDeckLeft: BlackCards([]BlackCard{
				{
					Text: "_. High five, bro.",
					Pick: 1,
				},
			}),
		},
		{
			desc: "pops last card from the deck and returns it, leaves deck empty",
			game: &Game{
				blackCards: BlackCards([]BlackCard{
					{
						Text: "_. High five, bro.",
						Pick: 1,
					},
				}),
			},
			expectedChoice: BlackCard{
				Text: "_. High five, bro.",
				Pick: 1,
			},
			expectedDeckLeft: BlackCards{},
		},
		{
			desc: "returns an error with an empty deck",
			game: &Game{
				blackCards: BlackCards{},
			},
			expectedDeckLeft: BlackCards{},
			expectedErr:      errBlackDeckEmpty,
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			chosen, err := tc.game.nextBlackCard()

			assert.Equal(t, tc.expectedErr, err)
			assert.Equal(t, tc.expectedChoice, chosen)
			assert.Equal(t, tc.expectedDeckLeft, tc.game.blackCards)
		})
	}
}

func TestDealWhiteCards(t *testing.T) {
	tests := []struct {
		desc         string
		game         *Game
		expectedGame *Game
	}{
		{
			desc: "deals missing white cards to every player in the game",
			game: &Game{
				whiteCardsPerPlayer: 4,
				whiteCards: []WhiteCard{
					{text: "The true meaning of Christmas."},
					{text: "Silence."},
					{text: "A spastic nerd."},
					{text: "Rehab."},
					{text: "An ugly face."},
					{text: "Hot cheese."},
					{text: "Shapeshifters."},
					{text: "Nicolas Cage."},
					{text: "Morgan Freeman's voice."},
					{text: "Poor life choices."},
				},
				players: Players([]*Player{
					&Player{
						id: "pl1",
						cards: []WhiteCard{
							{text: "Used panties."},
							{text: "Stormtroopers."},
						},
					},
					&Player{
						id: "pl2",
						cards: []WhiteCard{
							{text: "Hospice care."},
							{text: "Crystal meth."},
						},
					},
				}),
			},
			expectedGame: &Game{
				whiteCardsPerPlayer: 4,
				whiteCards: []WhiteCard{
					{text: "An ugly face."},
					{text: "Hot cheese."},
					{text: "Shapeshifters."},
					{text: "Nicolas Cage."},
					{text: "Morgan Freeman's voice."},
					{text: "Poor life choices."},
				},
				players: Players([]*Player{
					&Player{
						id: "pl1",
						cards: []WhiteCard{
							{text: "Used panties."},
							{text: "Stormtroopers."},
							{text: "The true meaning of Christmas."},
							{text: "Silence."},
						},
					},
					&Player{
						id: "pl2",
						cards: []WhiteCard{
							{text: "Hospice care."},
							{text: "Crystal meth."},
							{text: "A spastic nerd."},
							{text: "Rehab."},
						},
					},
				}),
			},
		},
	}

	for _, test := range tests {
		t.Run(test.desc, func(t *testing.T) {
			game := test.game

			game.dealWhiteCards()

			assert.Equal(t, test.expectedGame, game)
		})
	}
}

func TestCardsExcluding(t *testing.T) {
	tests := []struct {
		desc           string
		player         *Player
		cardsToExclude []WhiteCard
		expected       []WhiteCard
	}{
		{
			desc: "returns empty when Player has no cards",
			player: &Player{
				cards: WhiteCards{},
			},
			cardsToExclude: []WhiteCard{
				WhiteCard{text: "Crystal meth."},
			},
			expected: []WhiteCard{},
		},
		{
			desc: "returns original list of cards when no matches are found",
			player: &Player{
				cards: WhiteCards{
					WhiteCard{text: "Rehab."},
					WhiteCard{text: "Hot cheese."},
				},
			},
			cardsToExclude: []WhiteCard{
				WhiteCard{text: "Crystal meth."},
			},
			expected: []WhiteCard{
				WhiteCard{text: "Rehab."},
				WhiteCard{text: "Hot cheese."},
			},
		},
		{
			desc: "returns list with single card removed when a single match is found",
			player: &Player{
				cards: WhiteCards{
					WhiteCard{text: "Rehab."},
					WhiteCard{text: "Hot cheese."},
					WhiteCard{text: "Silence."},
				},
			},
			cardsToExclude: []WhiteCard{
				WhiteCard{text: "Hot cheese."},
			},
			expected: []WhiteCard{
				WhiteCard{text: "Rehab."},
				WhiteCard{text: "Silence."},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			res := tc.player.CardsExcluding(tc.cardsToExclude)
			assert.Equal(t, tc.expected, res)
		})
	}
}
