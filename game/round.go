package game

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

// Round represents a round in the game.
type Round struct {
	id        string
	czar      *Player
	players   Players
	blackCard BlackCard
	answers   map[string]WhiteCards
	czarsPick string
	winner    *Player

	sync.Mutex
}

// ID returns the UUID associated with the Round.
func (r *Round) ID() string {
	return r.id
}

// Czar returns the Round's czar.
func (r *Round) Czar() Player {
	return *r.czar
}

// Players returns the list of players who are playing
// this round. It excludes the czar.
func (r *Round) Players() Players {
	return r.players
}

// BlackCard returns this Round's black (question) card.
func (r *Round) BlackCard() BlackCard {
	return BlackCard{
		Text: r.blackCard.Text,
		Pick: r.blackCard.Pick,
	}
}

// PlayerPicked records the answers (picks) chosen by the given Player.
// It returns an error in case the number of answers received is different
// from the number of picks expected for the Round's BlackCard.
func (r *Round) PlayerPicked(p *Player, picks WhiteCards) error {
	if r.blackCard.Pick != len(picks) {
		return errInvalidAnswer
	}

	r.Lock()
	defer r.Unlock()

	r.answers[p.id] = picks
	for _, wc := range picks {
		p.cards.Delete(wc)
	}

	return nil
}

// CzarPicked records the round's winning user picked by the current Czar.
// It returns a non-nil error in case an invalid pick was received, or
// if somehow the answer wasn't found in the answers deck.
func (r *Round) CzarPicked(answers []WhiteCard) error {
	if len(answers) == 0 {
		return errInvalidPick
	}

	var pick string
	for uid, uAnswers := range r.answers {
		if len(uAnswers) != len(answers) {
			return errInvalidAnswer
		}

		for _, a := range answers {
			if a.text == uAnswers[0].text {
				pick = uid
				break
			}
		}
	}

	if pick == "" {
		return errPickNotFound
	}

	r.Lock()
	defer r.Unlock()
	r.czarsPick = pick

	return nil
}

// String returns a string representation of the Round.
func (r *Round) String() string {
	return fmt.Sprintf("%s is the new czar. Question:\n%s", r.czar, r.blackCard.Text)
}

// IsFinished returns whether or not the round is finished
// (i.e. if a winner has been picked).
func (r *Round) IsFinished() bool {
	return r.winner != nil
}

// Finish computes the Round's winner based on the received answers.
// It returns a non-nil error if:
// 1. there is already a winner defined (i.e. Finish() was called more
//    than once for the same Round)
// 2. the czar hasn't yet picked a winning set of answers
// 3. not all players have picked their answers
// 4. it can't find a winner (very unlikely)
func (r *Round) Finish() error {
	if r.winner != nil {
		return errRoundFinished
	}
	if r.czarsPick == "" {
		return errFinishWithoutPick
	}
	if len(r.answers) != len(r.players) {
		return errIncompleteRound
	}

	for playerID := range r.answers {
		if playerID == r.czarsPick {
			winner, err := r.players.find(playerID)
			if err != nil {
				return err
			}

			r.Lock()

			winner.score++
			r.winner = winner

			r.Unlock()

			break
		}
	}

	if r.winner == nil {
		return errWinnerUndefined
	}

	return nil
}

// Winner returns this Round's winner. It may return nil in case
// a winner hasn't been picked yet.
func (r *Round) Winner() *Player {
	r.Lock()
	defer r.Unlock()
	return r.winner
}

// AllAnswersReceived returns true when all players have picked
// their answers. It returns false otherwise.
func (r *Round) AllAnswersReceived() bool {
	r.Lock()
	defer r.Unlock()
	return len(r.players) == len(r.answers)
}

// Answers returns a map where the key is a player's UUID and the
// value is the list of answers picked by that player.
func (r *Round) Answers() map[string][]WhiteCard {
	r.Lock()
	defer r.Unlock()

	answers := make(map[string][]WhiteCard, len(r.answers))
	for uid, wCards := range r.answers {
		cards := make([]WhiteCard, len(wCards))
		for i, wc := range wCards {
			cards[i] = wc
		}
		answers[uid] = cards
	}
	return answers
}

// Status returns a string representing the current status of this Round.
// Possibilities are:
// 1. Waiting for players to pick their answers
// 2. All answers received, waiting for czar to pick the winner
// 3. Round is finished
func (r *Round) Status() string {
	winner := r.Winner()
	blackCard := r.blackCard.Text

	// Round isn't finished, let's return an in-flight status.
	if winner == nil {
		// If all answers were received, we're likely still waiting for
		// the Czar to pick the winning answer.
		if r.AllAnswersReceived() {
			var answerTexts []string
			for _, a := range r.answers {
				answerTexts = append(answerTexts, fmt.Sprintf("- %s", a))
			}
			sort.Strings(answerTexts)

			return fmt.Sprintf("All answers received, waiting for %s to pick the winner.\n\nQuestion: %s\n\n%s",
				r.czar.name,
				blackCard,
				strings.Join(answerTexts, "\n"),
			)
		}

		// If we're waiting for players to answer, status will include
		// the names of the players whose answers are missing.
		var missingNames []string
		for _, p := range r.players {
			if _, ok := r.answers[p.id]; !ok {
				missingNames = append(missingNames, p.name)
			}
		}

		return fmt.Sprintf("Current question is:\n%s\nStill waiting for answers from %s",
			blackCard,
			strings.Join(missingNames, ", "),
		)
	}

	status := blackCard
	answers := r.answers[winner.id]

	if strings.Contains(blackCard, "______") {
		for _, a := range answers {
			status = strings.Replace(status, "______", fmt.Sprintf("*%s*", a.text), 1)
		}
		return status
	}

	for _, a := range answers {
		status = fmt.Sprintf("%s *%s*.", status, a.text)
	}

	return status
}

// removePlayer removes the given player from the round.
// It returns a non-nil error if the player leaving the
// game is this round's czar.
func (r *Round) removePlayer(p *Player) error {
	r.Lock()
	defer r.Unlock()

	if r.czar.id == p.id {
		return errCzarCannotLeave
	}

	delete(r.answers, p.id)
	r.players = r.players.excluding(p)

	return nil
}
