package game

import "github.com/caiofilipini/go-against-humanity/store"

// Import creates a new Game from the given store.Game.
func Import(sg *store.Game) *Game {
	g := &Game{
		id:                  sg.ID,
		whiteCardsPerPlayer: sg.WhiteCardsPerPlayer,
		lastCzar:            sg.LastCzar,
	}

	blackCards := make([]BlackCard, len(sg.BlackCards))
	for i, bc := range sg.BlackCards {
		blackCards[i] = BlackCard{
			Text: bc.Text,
			Pick: bc.Pick,
		}
	}
	g.blackCards = BlackCards(blackCards)

	whiteCards := make([]WhiteCard, len(sg.WhiteCards))
	for i, wc := range sg.WhiteCards {
		whiteCards[i] = WhiteCard{
			text: wc,
		}
	}
	g.whiteCards = WhiteCards(whiteCards)

	for _, p := range sg.Players {
		pl := &Player{
			id:    p.ID,
			name:  p.Name,
			score: p.Score,
		}

		cards := make([]WhiteCard, len(p.Cards))
		for i, c := range p.Cards {
			cards[i] = WhiteCard{
				text: c,
			}
		}
		pl.cards = WhiteCards(cards)

		g.AddPlayer(pl)
	}

	return g
}
