package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"time"
)

func main() {
	token := flag.String("token", "1234", "auth token")
	responseCode := flag.Int("response", 200, "response code to use by default")
	fuzz := flag.Bool("fuzz", false, "introduce random failures")
	flag.Parse()

	http.HandleFunc(fmt.Sprintf("/%s/sendMessage", *token), func(w http.ResponseWriter, r *http.Request) {
		dump, err := httputil.DumpRequest(r, true)
		if err == nil {
			log.Println("Request received:")
			log.Println()
			log.Println(string(dump))
			log.Println()
		}

		// Let's fail sometimes
		if *fuzz && time.Now().Unix()%2 == 0 {
			log.Println("failing request on purpose")
			http.Error(w, "oops", http.StatusInternalServerError)
			return
		}

		w.WriteHeader(*responseCode)
	})

	log.Printf("using token %s, responding with %d, fuzz is %t\n", *token, *responseCode, *fuzz)
	log.Println("listening on :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
