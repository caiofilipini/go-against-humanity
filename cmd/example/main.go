package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/caiofilipini/go-against-humanity/game"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	log.Println("starting new game")
	g := game.NewGame()

	log.Printf("game %s started\n", g.ID())

	g.AddPlayer(game.NewPlayer("Marvin"))
	g.AddPlayer(game.NewPlayer("Arthur"))
	g.AddPlayer(game.NewPlayer("Ford"))
	zaphod := game.NewPlayer("Zaphod")
	g.AddPlayer(zaphod)

	fmt.Println("start new round? (y/n)")

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		resp := scanner.Text()
		if err := scanner.Err(); err != nil {
			log.Fatalf("failed to read input: %v\n", err)
		}
		if resp != "y" {
			os.Exit(0)
		}

		playRound(g)

		g.RemovePlayer(zaphod)

		fmt.Println()
		fmt.Println("start new round? (y/n)")
	}
}

func playRound(g *game.Game) {
	log.Println("starting new round")
	round, err := g.NewRound()
	if err != nil {
		log.Fatalf("can't create new round: %v\n", err)
	}

	log.Printf("round %s started\n", round.ID())
	log.Println(round)

	picks := round.BlackCard().Pick
	players := round.Players()
	var answers [][]game.WhiteCard

	for _, pl := range players {
		log.Println(round.Status())
		log.Println(pl.Cards())

		playersPicks := pl.Cards()[:picks]
		err = round.PlayerPicked(pl, playersPicks)
		if err != nil {
			log.Fatalf("can't register pick: %v\n", err)
		}
		answers = append(answers, playersPicks)

		if round.AllAnswersReceived() {
			log.Println(round.Status())
		}
	}

	log.Println(round.Status())
	fmt.Println()

	czarsPicks := answers[rand.Intn(len(answers))]
	err = round.CzarPicked(czarsPicks)
	if err != nil {
		log.Fatalf("can't register pick: %v\n", err)
	}

	err = round.Finish()
	if err != nil {
		log.Fatalf("can't finish round: %v\n", err)
	}

	winner := round.Winner()
	log.Printf("The winner of this round is %s!\n", winner)
	log.Println(round.Status())
	log.Println(g.ShowScores())

}
