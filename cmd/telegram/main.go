package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/boltdb/bolt"
	"github.com/caiofilipini/go-against-humanity/store"
	"github.com/caiofilipini/go-against-humanity/telegram"
)

const (
	dbName = "botagainsthumanity.db"

	// telegramAPI is the base URL for the bot API.
	telegramAPI = "https://api.telegram.org/bot"
)

func main() {
	pidfile := flag.String("pidfile", fmt.Sprintf("./%s.pid", os.Args[0]), "PID file name")
	logfile := flag.String("logfile", "", "log file name (when empty, logs will be sent to stdout)")
	apiURL := flag.String("api", telegramAPI, "Telegram API URL")
	flag.Parse()

	logDevice := os.Stdout
	if *logfile != "" {
		f, err := os.OpenFile(*logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalf("failed to create log file: %v\n", err)
		}
		logDevice = f
	}
	log.SetOutput(logDevice)

	if err := ioutil.WriteFile(*pidfile, []byte(fmt.Sprintf("%d\r\n", os.Getpid())), 0644); err != nil {
		log.Fatalf("failed to write pid file: %v\n", err)
	}

	certfile := getenv("CERT_FILE")
	keyfile := getenv("KEY_FILE")
	token := getenv("TELEGRAM_BOT_TOKEN")

	store, cleanup := openDB()
	defer cleanup()

	handler, err := telegram.NewHandler(*apiURL, token, store)
	if err != nil {
		log.Fatalf("failed to create handler: %v\n", err)
	}

	srv := http.Server{
		Addr:    ":8443",
		Handler: handler.Router(),
	}

	go func() {
		log.Printf("listening on %s\n", srv.Addr)
		if err := srv.ListenAndServeTLS(certfile, keyfile); err != nil {
			log.Fatalf("server error: %v\n", err)
		}
	}()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig

	log.Println("interrupted, shutting down")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		os.Remove(*pidfile)
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("failed to gracefully shutdown: %v\n", err)
	}
}

func getenv(key string) string {
	v := os.Getenv(key)
	if v == "" {
		log.Fatalf("%s is empty\n", key)
	}
	return v
}

func openDB() (store.Store, func()) {
	db, err := bolt.Open(fmt.Sprintf("./%s", dbName), 0600, nil)
	if err != nil {
		log.Fatalf("failed to open bolt DB file: %v\n", err)
	}

	bolt, err := store.NewBoltStore(db)
	if err != nil {
		db.Close()
		log.Fatalf("failed to create bolt store: %v\n", err)
	}

	return bolt, func() { db.Close() }
}
