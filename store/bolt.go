package store

import (
	"encoding/json"

	"github.com/boltdb/bolt"
)

var gamesBucket = []byte("games")

// NewBoltStore takes a bolt.DB, creates and returns a
// Store backed by Bolt.
func NewBoltStore(db *bolt.DB) (Store, error) {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(gamesBucket)
		return err
	})
	if err != nil {
		return nil, err
	}

	return &boltStore{
		db: db,
	}, nil
}

type boltStore struct {
	db *bolt.DB
}

var _ Store = (*boltStore)(nil)

// Put stores the given Game under the given Key.
// It returns a non-nil error in case it fails to store
// the game in Bolt.
func (b *boltStore) Put(k Key, g *Game) error {
	val, err := json.Marshal(g)
	if err != nil {
		return err
	}

	return b.db.Update(func(tx *bolt.Tx) error {
		return tx.Bucket(gamesBucket).Put([]byte(k), val)
	})
}

// Get loads the Game stored under the given Key.
// It returns a nil Game in case no matching Games were
// found.
// It returns a non-nil error in case it fails to retrieve
// the game from Bolt.
func (b *boltStore) Get(k Key) (*Game, error) {
	var g *Game

	err := b.db.View(func(tx *bolt.Tx) error {
		val := tx.Bucket(gamesBucket).Get([]byte(k))
		game, err := unmarshal(val)
		if err != nil {
			return err
		}

		g = game
		return nil
	})

	return g, err
}

// All returns all Games stored in Bolt.
// It returns a non-nil error in case it fails to load
// the games from Bolt.
func (b *boltStore) All() ([]*Game, error) {
	var games []*Game

	err := b.db.View(func(tx *bolt.Tx) error {
		return tx.Bucket(gamesBucket).ForEach(func(k, v []byte) error {
			g, err := unmarshal(v)
			if err != nil {
				return err
			}

			if g != nil {
				games = append(games, g)
			}
			return nil
		})
	})

	return games, err
}

func unmarshal(val []byte) (*Game, error) {
	if len(val) == 0 {
		return nil, nil
	}

	game := &Game{}
	if err := json.Unmarshal(val, game); err != nil {
		return nil, err
	}

	return game, nil
}
