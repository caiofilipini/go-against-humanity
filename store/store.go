package store

// Key represents a key used to identify the Game being stored.
type Key string

// Store defines the common operations of a Game store.
type Store interface {
	// Put persists the given Game under the given Key.
	// It returns a non-nil error in case it fails to persist.
	Put(Key, *Game) error

	// Get returns the Game stored under the given Key.
	// It returns a nil Game in case it doesn't find one matching
	// the Key. It returns a non-nil error in case it fails to
	// access the underlying storage.
	Get(Key) (*Game, error)

	// All returns all stored Games.
	// It returns an empty list in case no Games were stored.
	// It returns a non-nil error in case it fails to access
	// the underlying storage.
	All() ([]*Game, error)
}
