package store

// Game is the storable representation of a game.Game.
type Game struct {
	ID                  string      `json:"id"`
	ChatID              int64       `json:"chat_id"`
	WhiteCardsPerPlayer int         `json:"white_cards_per_player"`
	BlackCards          []BlackCard `json:"black_cards"`
	WhiteCards          []string    `json:"white_cards"`
	Players             []*Player   `json:"players"`
	LastCzar            int         `json:"last_czar"`
}

// Player is the storable representation of a game.Player.
type Player struct {
	ID     string   `json:"id"`
	ChatID int64    `json:"chat_id"`
	Name   string   `json:"name"`
	Score  int      `json:"score"`
	Cards  []string `json:"cards"`
}

// BlackCard is the storable representation of a game.BlackCard.
type BlackCard struct {
	Text string `json:"text"`
	Pick int    `json:"pick"`
}
