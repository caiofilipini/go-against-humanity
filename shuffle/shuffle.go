// Package shuffle provides a generic way for shuffling slices.
package shuffle

import (
	"math/rand"
	"time"
)

// Shuffler is the interface which types need to satisfy in order to
// be shuffled.
type Shuffler interface {
	// Len returns the number of elements in the slice.
	Len() int

	// Swap exchanges the elements under indices i and j.
	Swap(i, j int)
}

// Shuffle permutates the elements in s by relying on rand.Intn to
// generate random indices and then swapping the elements around.
func Shuffle(s Shuffler) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := s.Len() - 1; i > 0; i-- {
		j := r.Intn(i + 1)
		s.Swap(i, j)
	}
}
